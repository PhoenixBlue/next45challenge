# MARS ROVER CHALLENGE
## 1 Usage
The robot will first be positioned as defined in the second line of the instruction file, before moving ,following instructions on the third line.

The input to the program is a text file, "source.txt", placed in the same directory as the class files.

Instructions can be edited at will, following a specific protocol. 
The first line defines maximum coordinates of the robot; it is assumed that those coordinates must have the same number of digits for the program to understand it.

The second line should have the initial coordinates of the robot and its initial direction (W-west, E-east, N-north or S-south). Initial coordinates are set using same principles as the first line, and the initial coordinated will be separated from them by a plank space.
The third line specifies movement instructions to displace the robot in its environment. Possible values of each character are M-move, L-left or R-right.

## 2 Design 
A robot class is created, with maximum coordinates, current coordinates and direction as members. 
The main class will be responsible for reading the source file, before setting up a Robot object.
Maximum coordinates are included to make sure that the robot does not go out of its boundaries.


## 3 Testing
To perform tests on the application, an additional line must be added, to specify the expected results. All items are separated by blank spaces.
To run the application into testing mode, on console the user must add the argument 'test'
The first item is a boolean (true or false), corresponding to the result of parsing maximum coordinates, initial coordinates and direction of the robot.
The second item, represents the expected final x-position of the robot, while the third item represents the final y-coordinate. 