import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class next45Challenge {
    public static void main(String[] args) {
        List<String> lines = new ArrayList<>();
        String instructions;
        Robot marsRover = new Robot();
        boolean valid = false;

        lines = readInstructions("source.txt");
        valid = marsRover.parseInstructions(lines);
        instructions = lines.get(2);
        if (!valid) {
            System.out.println("Parsing failed");
        } else {
            marsRover.display();
            marsRover.move(instructions);
            marsRover.display();
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("test")) {
            System.out.println("testing");
            try {
                String resultLine = lines.get(3);
                String[] results = resultLine.split(" ");

                assert valid == (Boolean.valueOf(results[0]));
                if (valid) {
                    assert marsRover.pos_x == Integer.valueOf(results[1]);
                    assert marsRover.pos_y == Integer.valueOf(results[2]);
                    assert marsRover.direction == results[3].charAt(0);

                }
            } catch (Exception e) {
                System.out.println("Error on test:" + e.getMessage());
            }
        }

    }

    /**
     * READ INSTRUCTIONS ================== Read instructions for the robot
     * 
     * @filename: path of the file to read from. If the filename is 'null', then
     *            read from console Return a list of 'lines' read from file/console
     */
    static List<String> readInstructions(String filename) {
        // Read instructions from file
        // If filename is null, then read from console
        List<String> lines = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                lines.add(sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

}