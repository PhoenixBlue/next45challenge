import java.io.FileReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Robot {
    int max_x;
    int max_y;
    int pos_x;
    int pos_y;
    char direction;

    public Robot() {
        max_x = 0;
        max_y = 0;
        pos_x = 0;
        pos_y = 0;
        direction = 'N';
    }

    /**
     * VALIDATE INSTRUCTIONS ====================== Make sure that the list of
     * instructions in the list are what to expect
     * 
     */
    public boolean parseInstructions(List<String> lines) {
        if (lines.size() < 2) {
            // throw new Exception("List of instructions not complete");
            return false;
        }

        String cLine;
        int nbDigits;

        /* Get maximum coordinates */
        cLine = lines.get(0); // On first line...
        nbDigits = cLine.length(); // Get number of digits

        if (nbDigits % 2 != 0)
            return false;

        // Convert coordinates to integer
        try {
            max_x = Integer.parseInt(cLine.substring(0, nbDigits / 2));
            max_y = Integer.parseInt(cLine.substring(nbDigits / 2));
        } catch (Exception e) {
            return false;
        }

        /* Get initial position and direction */
        String strCoord;
        char tmpDirection;

        cLine = lines.get(1);
        strCoord = cLine.substring(0, cLine.indexOf(' ')).trim();
        nbDigits = strCoord.length();
        tmpDirection = cLine.substring(cLine.indexOf(' ')).trim().toUpperCase().charAt(0);
        String validDirections = "WENS";
        if (validDirections.indexOf(tmpDirection) < 0) { // Validate direction
            return false;
        }
        direction = tmpDirection;
        try { // Get initial position
            pos_x = Integer.parseInt(strCoord.substring(0, nbDigits / 2));
            pos_y = Integer.parseInt(strCoord.substring(nbDigits / 2));
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    /** DISPLAY */
    public void display() {
        System.out.println("max_x: " + String.valueOf(max_x));
        System.out.println("max_y: " + String.valueOf(max_y));
        System.out.println("pos_x: " + String.valueOf(pos_x));
        System.out.println("pos_y: " + String.valueOf(pos_y));
        System.out.println("direction: " + String.valueOf(direction));
    }

    public void move(String movement) {
        // Move around as per the instructions
        for (int z = 0; z < movement.length(); z++) {
            char c = movement.charAt(z);
            switch (c) {
            case 'M': { // Move
                switch (direction) {
                case 'N':
                    pos_y++;
                    break;
                case 'S':
                    pos_y--;
                    break;
                case 'E':
                    pos_x++;
                    break;
                case 'W':
                    pos_x--;
                    break;
                default:
                    break;
                }
                break;
            }
            case 'L': { // Turn Left
                switch (direction) {
                case 'N':
                    direction = 'W';
                    break;
                case 'S':
                    direction = 'E';
                    break;
                case 'E':
                    direction = 'N';
                    break;
                case 'W':
                    direction = 'S';
                    break;
                default:
                    break;
                }
                break;
            }
            case 'R': { // Turn Right
                switch (direction) {
                case 'N':
                    direction = 'E';
                    break;
                case 'S':
                    direction = 'W';
                    break;
                case 'E':
                    direction = 'S';
                    break;
                case 'W':
                    direction = 'N';
                    break;
                default:
                    break;
                }
                break;
            }
            default:
                break;
            }
            // Do not go out of
            if (pos_x > max_x)
                pos_x = max_x;
            if (pos_y > max_y)
                pos_y = max_y;
            if (pos_x < 0)
                pos_x = 0;
            if (pos_y < 0)
                pos_y = 0;
        }
    }
}